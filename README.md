## `Emmet Api SDK For PHP`

 功能描述
----

* 微信进件提交


代码仓库
----
emmet-api 为开源项目，允许把它用于任何地方，不受任何约束，欢迎 fork 项目。
* Gitee 托管地址：https://gitee.com/mirenfawei11/emmet.git


文件说明
----
|文件名|类名|描述|
|---|---|---|
|  EmtWxapply.php  |  emmet\api  |  微信进件提交  |
|  EmtWxbank.php  |  emmet\api  |  微信银行信息  |
|  EmtWxmedia.php  |  emmet\api |  微信媒体提交获取media_id  |


安装使用
----

1.1 通过 Composer 来管理安装
```shell
#  首次安装 开发版本（开发）
composer require emmet/emmet-api





## 快速使用

* 将获取到的[app_id]和[app_secret]按照以下方式填写到配置文件中 

~~~
  
~~~
* 以查询[获取开户银行的银行名称为例]()接口为例
~~~
    use emmet\api;
      //配置文件中需要的内容
    $data =  [
        'app_id'        =>  '***********',
        'app_secret'    =>  '***********',
    ];
    $bank  = new Loader('wxbank',$data);
    $result = $bank->getAccountBank('农业银行');
~~~


## 返回数据
`emmet-api`所有的接口返回数据为`json`格式，通用规范如下：

| 名称 | 类型 | 说明 |
| --- | --- | --- |
| code | int | 返回码,200 表示成功 其它表示失败 |
| message| string | 返回提示信息 |
| data| object | 返回数据 |


**Emmet Api SDK 版权声明**
* 此SDK基于`MIT`协议发布，任何人可以用在任何地方，不受约束。