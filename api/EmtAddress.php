<?php
namespace emmet\api;
// +----------------------------------------------------------------------
// | 作  者: Liu
// +----------------------------------------------------------------------
// | 日  期: 2021/11/9 0009 9:21
// +----------------------------------------------------------------------
// | 备  注: 
// +----------------------------------------------------------------------

use emmet\api\Lib\Common;
use emmet\api\Lib\Tools;

class EmtAddress extends Common{

    const GET_PROVINCE_LIST_URL = '/v1/mchaddress/getProvinceList';//获取省份列表url
    const GET_CITY_LIST_URL     = '/v1/mchaddress/getCityList';//获取城市列表url
    const GET_COUNTY_LIST_URL   = '/v1/mchaddress/getCountyList';//获取县列表url
    const GET_TOWN_LIST_URL     = '/v1/mchaddress/getTownList';//获取城镇列表url
    const GET_ADDRESS_LIST      = '/v1/mchaddress/getAddressList';//获取全部地址列表

    /**
     *
     * @descripton 获取省份列表
     * @return false|mixed
     */
    public function getProvinceList(){
        if (!$this->getAuthorization()){
            return false;
        }
        $result = Tools::httpGet(self::DOMAIN_NAME.self::GET_PROVINCE_LIST_URL,$this->authorization);
        if ($result){
            $json = json_decode($result,true);
            return $json;
        }
        return false;
    }

    /**
     *
     * @descripton 获取城市列表
     * @param $parent_code 父级parent_code
     * @return false|mixed
     */
    public function getCityList($parent_code){
        if (!$this->getAuthorization()){
            return false;
        }
        $result = Tools::httpGet(self::DOMAIN_NAME.self::GET_CITY_LIST_URL.'?parent_code='.$parent_code,$this->authorization);
        if ($result){
            $json = json_decode($result,true);
            return $json;
        }
        return false;
    }

    /**
     *
     * @descripton 获取县列表
     * @param $parent_code 父级parent_code
     * @return false|mixed
     */
    public function getCountyList($parent_code){
        if (!$this->getAuthorization()){
            return false;
        }
        $result = Tools::httpGet(self::DOMAIN_NAME.self::GET_COUNTY_LIST_URL.'?parent_code='.$parent_code,$this->authorization);
        if ($result){
            $json = json_decode($result,true);
            return $json;
        }
        return false;
    }

    /**
     *
     * @descripton 获取城镇列表
     * @param $parent_code 父级parent_code
     * @return false|mixed
     */
    public function getTownList($parent_code){
        if (!$this->getAuthorization()){
            return false;
        }
        $result = Tools::httpGet(self::DOMAIN_NAME.self::GET_TOWN_LIST_URL.'?parent_code='.$parent_code,$this->authorization);
        if ($result){
            $json = json_decode($result,true);
            return $json;
        }
        return false;
    }

    /**
     *
     * @descripton 获取全部地址列表
     * @return false|mixed
     */
    public function getAddressList(){
        if (!$this->getAuthorization()){
            return false;
        }
        $result = Tools::httpGet(self::DOMAIN_NAME.self::GET_ADDRESS_LIST,$this->authorization);
        if ($result){
            $json = json_decode($result,true);
            return $json;
        }
        return false;
    }

}